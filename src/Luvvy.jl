module Luvvy

export Director, Route

export Go!, Listening!, Route!, Write!, Handle!

using Actors, HTTP, Sockets
import Actors: hear, inbox
import HTTP: Connection, Transaction, Request, Stream, hasheader, setheader
import Sockets: TCPServer

const HTTPServer = HTTP.Servers.Server

mutable struct Server
    dir::Id
    srv::HTTPServer

    Server(dir) = new(dir)
    Server(dir, srv) = new(dir, srv)
end

struct Listening! end

struct Connected!
    sk::TCPSocket
    host::String
    port::String
end

Base.iterate(srv::TCPServer, state=nothing) = let sk = accept(srv)
    isnothing(sk) ? sk : (sk, nothing)
end

function Actors.listen!(s::Scene{Server})
    dir = my(s).dir
    my(s).srv = srv = HTTPServer(nothing, listen(8888), "localhost", "8888")

    say(s, dir, Listening!())
    async(s) do s
        try
            for sk in srv.server
                say(s, dir, Connected!(sk, srv.hostname, srv.hostport))
            end
        finally
            leave!(s)
            close(srv)
        end
    end

    for msg in inbox(s)
        hear(s, msg)
    end
end

function dieing_breath(s::Scene{Server}, ex, env)
    a = me(s)

    close(my(s).srv)

    say(s, minder(s), Died!(a, my_ref(a)[]))
end

hear(s::Scene{Server}, ::Leave!) = close(my(s).srv)

struct Route
    pattern::Regex
    handler::Function
end

mutable struct Director
    play::Id
    routes::Vector{Route}
    srv::Id{Server}

    Director(play, routes=[]) = new(play, routes)
end

struct Go! end

hear(s::Scene{Director}, ::Go!) =
    my(s).srv = invite!(s, Server(me(s)))

hear(s::Scene{Director}, msg::Listening!) = say(s, my(s).play, msg)
hear(s::Scene{Director}, conn::Connected!) =
    invite!(s, ReaderWriter(conn, invite!(s, Router(my(s).routes))))

mutable struct LiveRoute
    route::Route
    handler::Union{Id, Nothing}
end

struct Router
    routes::Vector{LiveRoute}
end

Router(routes::Vector{Route}) = Router(map(routes) do r
    LiveRoute(r, nothing)
end)

struct Route!
    http::Stream
    rw::Id
end

struct Handle!
    http::Stream
    rw::Id
    matched::RegexMatch
end

function hear(s::Scene{Router}, msg::Route!)
    req = msg.http.message

    for live in my(s).routes
        matched = match(live.route.pattern, req.target)

        isnothing(matched) && continue

        if isnothing(live.handler)
            live.handler = invite!(s, live.route.handler(msg.rw))
        end

        say(s, live.handler, Handle!(msg.http, msg.rw, matched))
        return
    end

    @say_info s "Not found: $(req.target)"
    req.response.status = 404
    say(s, msg.rw, Write!(msg.http, b"404"))
end

mutable struct ReaderWriter
    conn::Connected!
    router::Id{Router}
    alive::Ref{Bool}
end

ReaderWriter(conn, router) = ReaderWriter(conn, router, true)

function Actors.listen!(s::Scene{ReaderWriter})
    conn = let c = my(s).conn
        c2 = Connection(c.sk)
        c2.host, c2.port = c.host, c.port
        c2
    end
    router = my(s).router
    rw = me(s)
    alive = my(s).alive
    tid = Threads.threadid()

    @say_info s "New connection: $conn"
    read_task = async(s) do s
        while alive[]
            req = Request()
            http = Stream(req, Transaction(conn))

            startread(http)

            req.response.status = 200
            if hasheader(req, "Connection", "close")
                setheader(req.response, "Connection" => "close")
                alive[] = false
            end

            @say_info s "New Request: $req"

            req.body = read(http)

            begin # Copied from HTTP.closeread(http) sans notify(poolcondition)
                if HTTP.Streams.incomplete(http)
                    close(http.stream)
                    throw(EOFError())
                end

                t = http.stream
                HTTP.@require isreadable(t)

                t.c.readbusy = false
                t.c.readcount += 1
                notify(t.c.readdone)

                HTTP.@ensure !isreadable(t)
            end

            say(s, router, Route!(http, rw))
        end
    end

    for msg in inbox(s)
        hear(s, msg)
    end

    wait(read_task)
end

function dieing_breath(s::Scene{ReaderWriter}, ex, env)
    close(my(s).conn.sk)

    say(s, minder(s), Died!(me(s), subject(s)))
end

struct Write!{T}
    http::Stream
    data::T
end

function hear(s::Scene{ReaderWriter}, msg::Write!)
    alive = my(s).alive
    conn = my(s).conn
    http = msg.http

    async(s) do s
        startwrite(http)
        write(http, msg.data)
        begin # closewrite(http) sans notify(poolcondition)
            if iswritable(http)
                HTTP.Streams.closebody(http)

                t = http.stream
                t.c.writebusy = false
                t.c.writecount += 1
                notify(t.c.writedone)

                HTTP.@ensure !iswritable(t)
            end

            if hasheader(http.message, "Connection", "close") ||
                hasheader(http.message, "Connection", "upgrade") ||
                http.message.version < v"1.1" &&
                !hasheader(http.message, "Connection", "keep-alive")

                close(http.stream)
            end
        end

        if !alive[] && http.stream.sequence >= conn.sequence
            close(conn.io)
            leave!(s)
        end
    end
end

hear(s::Scene{ReaderWriter}, ::Leave!) = close(my(s).conn.sk)

end # module
