# Luvvy - Julia Actors Web Framework

The original Luvvy was renamed to
[Actors.jl](https://gitlab.com/Palethorpe/actors.jl).

For now this is a web framework based on Actors.jl.

## Hello, World!

```julia
using Actors, Luvvy
import Actors: hear

struct HelloWorld end

struct Handler end

function hear(s::Scene{Handler}, msg::Route!)
    say(s, msg.rw, Write!(msg.http, b"Hello, World!"))
end

function hear(s::Scene{HelloWorld}, ::Genesis!)
    dir = enter!(s, Director(me(s), [
        Route(r"/", (_) -> Handler())
    ]))

    say(s, dir, Go!())
end

hear(s::Scene{HelloWorld}, ::Listening!) =
    @say_info s "Listening on localhost:8888"

play!(HelloWorld())
```
