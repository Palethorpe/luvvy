using Test

using Actors, Luvvy, HTTP
import Actors: hear

notifier = Channel()

struct FooHandler end

function hear(s::Scene{FooHandler}, msg::Handle!)
    say(s, msg.rw, Write!(msg.http, b"Bar!"))
end

struct FoobarHandler end

hear(s::Scene{FoobarHandler}, msg::Handle!) =
    say(s, msg.rw, Write!(msg.http, msg.matched.captures[1]))

struct ConnectionsPlay end

function hear(s::Scene{ConnectionsPlay}, ::Genesis!)
    routes = [Route(r"/foo$", (_) -> FooHandler()),
              Route(r"/foo\?bar=(.*)", (_) -> FoobarHandler())]
    dir = invite!(s, Director(me(s), routes))

    say(s, dir, Go!())
end

function hear(s::Scene{ConnectionsPlay}, msg::Listening!)
    put!(notifier, msg)

    take!(notifier)

    say(s, stage(s), Leave!())
end

@testset "Connections" begin
    host = "http://localhost:8888"
    p = @async play!(ConnectionsPlay())

    take!(notifier)
    resp = HTTP.get(host; status_exception=false)
    @test resp.status == 404
    @test "404" == String(resp.body)

    resp = HTTP.get(host * "/foo")
    @test resp.status == 200
    @test "Bar!" == String(resp.body)

    resp = HTTP.get(host * "/foo?bar=baz")
    @test resp.status == 200
    @test "baz" == String(resp.body)

    put!(notifier, Leave!())
    wait(p)
end
